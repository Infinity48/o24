# O24
A stack-based interpreted assembly-like semi-esoteric programming language. The reference implementation can be used as both a library and an independent program.

## Concepts
### Stack
The stack is the main data storage area. All operations except flow control, `pop` and `popt` push to the stack.

### Variable
Variables are the secondary storage area. They can contain either a value or a table.

### Table
Tables are collections of multiple values. They can have arbitrary integer keys.

### Pop
Pops the value at the top of the stack into the specified variable.

### Flow control
`j` is jump.  
`jeq` is jump if equal to.  
`jlt` is jump if less than.  
`jgt` is jump if greater than.  

`c` is call. It works like jump except it pushes the current line onto the call stack (different from the main stack).  

`ret` is return. It pops the top of the call stack and jumps to that line. (Or put simply, it returns.)

All operations use a line number.

## Syntax guide
### Stack operations
```
push <value>
pop <variable>
pusht <table> <index>
popt <table> <index>
```

### Flow control
Conditional operations compare to the top of the stack.
```
j <line>
jeq <line> <value>
jgt <line> <value>
jlt <line> <value>
c <line>
ceq <line> <value>
cgt <line> <value>
clt <line> <value>
ret
```

### Arithmetic and logic
All operations pop the value at the top of the stack and use it as their first operator.
```
add <value>
sub <value>
mul <value>
div <value>
mod <value>
not
and <value>
or <value>
xor <value>
ls <value>
rs <value>
```

### Other
$ before variable name can be used in place of value or line. Eg: `push $hello`  
; at the beginning of a line is a comment.