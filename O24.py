import sys

code = [None]
line = 1
stack = []
calls = []
variables = {}

def close():
    global code, line, stack, calls, variables
    code = [None]
    line = 1
    stack = []
    calls = []
    variables = {}

def load(path):
    close()
    with open(path, 'r') as f:
        for l in f.read().splitlines():
            code.append(l.split(' '))    

def step():
    global line
    def sub_value(value):
        if value.startswith('$'):
            return variables[value[1::]]
        return int(value)
    
    curcode = code[line]
    match curcode[0]:
        # Stack operations.
        case 'push':
            stack.append(sub_value(curcode[1]))
        case 'pop':
            if len(curcode) == 1:
                stack.pop()
            else:
                variables[curcode[1]] = stack.pop()
        case 'pusht':
            stack.append(variables[curcode[1]][int(curcode[2])])
        case 'popt':
            index = int(curcode[2])
            if curcode[1] in variables and isinstance(variables[curcode[1]], dict):
                variables[curcode[1]][index] = stack.pop()
            else:
                variables[curcode[1]] = {}
                variables[curcode[1]][index] = stack.pop()
        # Arithmetic and logic.
        case 'add':
            stack.append(stack.pop() + sub_value(curcode[1]))
        case 'sub':
            stack.append(stack.pop() - sub_value(curcode[1]))
        case 'mul':
            stack.append(stack.pop() * sub_value(curcode[1]))
        case 'div':
            stack.append(stack.pop() // sub_value(curcode[1]))
        case 'mod':
            stack.append(stack.pop() % sub_value(curcode[1]))
        case 'not':
            stack.append(~stack.pop())
        case 'and':
            stack.append(stack.pop() & sub_value(curcode[1]))
        case 'or':
            stack.append(stack.pop() | sub_value(curcode[1]))
        case 'xor':
            stack.append(stack.pop() ^ sub_value(curcode[1]))
        case 'ls':
            stack.append(stack.pop() << sub_value(curcode[1]))
        case 'rs':
            stack.append(stack.pop() >> sub_value(curcode[1]))
        # Flow control.
        case 'j':
            line = sub_value(curcode[1]) - 1
        case 'jeq':
            if stack[-1] == int(curcode[2]):
                line = sub_value(curcode[1]) - 1
        case 'jgt':
            if stack[-1] > int(curcode[2]):
                line = sub_value(curcode[1]) - 1
        case 'jlt':
            if stack[-1] < int(curcode[2]):
                line = sub_value(curcode[1]) - 1
        case 'c':
            calls.append(line)
            line = sub_value(curcode[1]) - 1
        case 'ceq':
            if stack[-1] == int(curcode[2]):
                calls.append(line)
                line = sub_value(curcode[1]) - 1
        case 'cgt':
            if stack[-1] > int(curcode[2]):
                calls.append(line)
                line = sub_value(curcode[1]) - 1
        case 'clt':
            if stack[-1] < int(curcode[2]):
                calls.append(line)
                line = sub_value(curcode[1]) - 1
        case 'ret':
            line = calls.pop()
    line += 1

def main(argv):
    load(argv[1])

    variables['stdout'] = ''
    
    while line < len(code):
        step()
        if variables['stdout']:
            sys.stdout.write(chr(variables['stdout']))
            variables['stdout'] = ''

if __name__ == '__main__':
	main(sys.argv)